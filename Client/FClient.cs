﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Server;

namespace Client
{
    public partial class FClient : Form
    {
        IPEndPoint ip;
        Socket client;
        List<Button> choiceList;
        List<string> playerStringList;
        List<Panel> panelList;
        System.Windows.Forms.Timer timer1;
        Stopwatch stopwatch;
        Rank rank;
        Quiz q;
        string playerName;
        string playerString;
        int totalQuiz;
        int quiz_num;
        long timeout = 5000;
        IPAddress IP;
        public FClient()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxInputIP.Text.Equals("Enter Host's IP") || textBoxInputIP.Text.Equals(""))
            {
                textBoxInputIP.Text = "127.0.0.1";
            }
            try
            {
                IP = IPAddress.Parse(textBoxInputIP.Text);
            }
            catch
            {
                //kiêm tra ip
                //todo
            }

            //MessageBox.Show("Bạn đã tham gia thành công, hãy sẵn sàng...", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Question);

            textBoxInputIP.Visible = false;
            button1.Visible = false;

            Connect();
            TextBox.CheckForIllegalCrossThreadCalls = false;
            RichTextBox.CheckForIllegalCrossThreadCalls = false;
            stopwatch = new Stopwatch();
            SetupTimer();
            SetupChoiceButton();
            SetupPanelList();
            SetupProgressBar();
            quiz_num = 0;
        }

        //Gửi yêu cầu kết nối đến server tại địa chỉ trong ip
        void Connect()
        {
            ip = new IPEndPoint(IP, 9999);
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            try
            {
                client.Connect(ip);
            }
            catch (Exception e) { }
            byte[] data = new byte[1024];

            //Nhận thông tin cấu hình từ server: thời gian trả lời câu hỏi, tổng số câu và username được server đặt
            ReceiveConfigInf();

            //Thread nhận câu hỏi
            Thread listen = new Thread(ReceiveQuiz)
            {
                IsBackground = true
            };
            listen.Start();
        }

        void ClientClose()
        {
            client.Close();
        }

        //Hàm nhận câu hỏi
        private void ReceiveQuiz()
        {
            try
            {
                while (true)
                {
                    //quiz_num ghi nhận số câu hỏi đã nhận được
                    if (quiz_num > 0)
                    {
                        //Nhận kết quả xếp hạng
                        ReceiveRank();
                        //Kết thúc khi đã hoàn thành toàn bộ câu hỏi 
                        if (quiz_num == totalQuiz)
                        {
                            EndGame();

                        }
                    }
                    //Nhận dữ liệu câu hỏi dưới dạng byte[]
                    byte[] data = new byte[1024];
                    NetworkStream ns = new NetworkStream(client);
                    StreamReader sr = new StreamReader(ns);
                    StreamWriter sw = new StreamWriter(ns);
                    byte[] packsize_byte = new byte[2];
                    int recv = ns.Read(packsize_byte, 0, 2);
                    int packsize = BitConverter.ToInt16(packsize_byte, 0);
                    recv = ns.Read(data, 0, packsize);
                    sr.Close();
                    sw.Close();
                    ns.Close();

                    if (recv != 0)
                    {
                        //Chuyển dữ liệu byte[] nhận được thành câu hỏi Quiz q
                        q = new Quiz(data);

                        //Tăng số lượng câu hỏi đã nhận được
                        quiz_num++;
                        if (quiz_num != 1)
                        {
                            HideRanking();
                        }
                        //Bắt đầu đếm giờ trả lời
                        StartTimer();
                        //Hiển thị câu hỏi q lên màn hình
                        ShowQuiz(q);
                    }

                }
            }
            catch (Exception e) { }
        }

        //Xử lý kết thúc, chưa xong
        private void EndGame()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    EndGame();
                });
            }
            else
            {
                string caption = "Message";
                string message = "The game has ended" + Environment.NewLine;
                message += "Do you want to play again?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                // Displays the MessageBox.
                result = MessageBox.Show(message, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    // Closes the parent form.
                    this.Close();
                    Dispose();
                    Application.Restart();
                }
                else
                {
                    Dispose();
                }
            }
        }

        //Tạo các button chọn đáp án
        private void SetupChoiceButton()
        {
            choiceList = new List<Button>();
            //Vị trí của button đầu tiên (Đáp án A)
            int locationX = 35;
            int locationY = 290;
            int distanceX = 360;
            int distanceY = 90;
            Size choiceSize = new Size(350, 80);
            for (int i = 0; i < 4; i++)
            {
                //Thiết lập font chữ, kích thước, vị trí cho các đáp án
                Button new_button = new Button()
                {
                    Font = new Font("Cambria", 11),
                    Parent = this,
                    Size = choiceSize,
                    Location = new Point(locationX, locationY),
                    Visible = false,
                };

                //Thêm sự kiện xử lý khi chọn đáp án 
                new_button.Click += new EventHandler(SetupReply);

                //Quản lý button bằng list chung
                choiceList.Add(new_button);
            }
            //Thiết lập vị trí các đáp án
            choiceList[3].Location = new Point(locationX + distanceX, locationY + distanceY);
            choiceList[2].Location = new Point(locationX, locationY + distanceY);
            choiceList[1].Location = new Point(locationX + distanceX, locationY);
        }


        //Bắt đầu đếm giờ
        void StartTimer()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    StartTimer();
                });
            }
            else
            {
                //Khởi động bộ đếm Timer (Toolbox)
                timer1.Start();
                if (stopwatch.IsRunning)
                {
                    stopwatch.Start();
                }
                else
                {
                    //Restart() khi bộ đếm dừng hoạt động
                    stopwatch.Restart();
                }
            }
        }
        //Hiển thị câu hỏi, đáp án và thanh tiến trình
        void ShowQuiz(Quiz q)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    ShowQuiz(q);
                });
            }
            else
            {
                //Progress Bar
                progressBar1.Visible = true;
                //Question
                label1.Visible = true;
                label1.Font = new Font("Cambria", 13);
                label1.Text = q.question;
                int x = (this.Width - label1.Width) / 2;
                int y = this.Height / 8;
                label1.Location = new Point(x, y);
                //Button
                for (int i = 0; i < choiceList.Count; i++)
                {
                    choiceList[i].Visible = true;
                    choiceList[i].Text = q.choices[i];
                }

            }
        }

        //Ẩn các thành phần được hiển thị trong hàm ShowQuiz()
        void HideQuiz()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    HideQuiz();
                });
            }
            else
            {
                //Progress Bar
                progressBar1.Visible = false;
                //Question
                label1.Visible = false;
                //Button
                for (int i = 0; i < choiceList.Count; i++)
                {
                    choiceList[i].Visible = false;
                }
            }
        }

        //Xử lý sự kiện cho đồng hồ
        //Sau mỗi khoảng thời gian Interval được đặt trước, sự kiện này sẽ được gọi 1 lần
        private void Timer1_Tick(object sender, EventArgs e)
        {
            //Cập nhập UI của thanh tiến trình
            if (progressBar1.Value < progressBar1.Maximum)
            {
                progressBar1.Value += 1;
            }
            //Tạo Reply cho server khi hết thời gian
            else
            {
                HideQuiz();
                SetupReply(null, null);
            }
        }

        private void Form1_Load(object sender, EventArgs e) { }

        //Tạo Reply trả lời cho server, bao gồm kết quả, thời gian trả lời 
        private void SetupReply(Object sender, EventArgs e)
        {
            //Dừng đếm ngược
            stopwatch.Stop();
            //Dừng đồng hồ
            timer1.Stop();
            //Reset giá trị của thanh tiến trình
            progressBar1.Value = 0;
            //Tạo giá trị mặc định cho trường hợp hết thời gian
            bool isRight = false;
            long timeTaken = timeout;
            //Trường hợp hoàn thành câu hỏi trong thời gian cho phép
            if (sender != null)
            {
                Button clickedBtn = sender as Button;
                //Đáp án đã chọn
                isRight = clickedBtn.Text == q.choices[q.answer];
                //Thời gian trả lời
                timeTaken = stopwatch.ElapsedMilliseconds;
            }
            //Tạo Reply từ các thông tin trên và gửi đến server
            Reply reply = new Reply(isRight, timeTaken);
            SendReply(reply);
            //Ẩn câu hỏi
            HideQuiz();

        }

        //Gửi Reply
        private void SendReply(Reply reply)
        {
            byte[] data = reply.getBytes();
            NetworkStream ns = new NetworkStream(client);
            StreamReader sr = new StreamReader(ns);
            StreamWriter sw = new StreamWriter(ns);
            byte[] packsize = new byte[2];
            packsize = BitConverter.GetBytes(reply.getSize());
            ns.Write(packsize, 0, 2);
            ns.Write(reply.getBytes(), 0, reply.getSize());
            ns.Flush();
            sr.Close();
            sw.Close();
            ns.Close();
        }

        //Hiển thị bảng xếp hạng
        private void ShowRanking()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    ShowRanking();
                });
            }
            else
            {
                //Tìm vị trí của Player hiện tại trong danh sách xếp hạng
                int index = GetPlayerInfFromString(rank.ranking);
                for (int i = 0; i < rank.getNum(); i++)
                {
                    panelList[index].BackColor = Color.Transparent;
                }
                //Đổi màu cho dòng thông tin này
                panelList[index].BackColor = Color.SkyBlue;
                rankPanel.Visible = true;
                //Đưa thông tin xếp hạng vào các panel
                for (int i = 0; i < rank.getNum(); i++)
                {
                    panelList[i].Visible = true;
                    AddTextIntoPanel(panelList[i], i + 1 + ". " + rank.ranking[i]);
                }
            }
        }

        //Đưa thông tin xếp hạng vào 1 panel cụ thể
        private void AddTextIntoPanel(Panel panel, string text)
        {
            panel.Controls[0].Text = text;
            panel.Refresh();
        }

        //Ẩn bảng xếp hạng
        private void HideRanking()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    HideRanking();
                });
            }
            else
            {
                rankPanel.Visible = false;
                for (int i = 0;
                    i < rank.getNum(); i++)
                {
                    panelList[i].Visible = false;
                }
            }
        }

        //Cài đặt bảng xếp hạng
        private void SetupPanelList()
        {
            int panelY = 0;
            panelList = new List<Panel>
            {
                panel1,
                panel2,
                panel3,
                panel4
            };
            for (int i = 0; i < panelList.Count; i++)
            {
                panelList[i].Size = new Size(rankPanel.Width, rankPanel.Height / 10);
                panelList[i].Left = (rankPanel.Width - panelList[i].Width) / 2;
                panelList[i].Top = (rankPanel.Height - panelList[i].Height) / 2 + panelY;
                panelY += panelList[i].Height;

                Label label = new Label
                {
                    Font = new Font("Cambria", 16),
                    AutoSize = false,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Dock = DockStyle.Fill,
                };
                panelList[i].Controls.Add(label);
            }
        }

        //Tìm vị trí của Player trong bảng xếp hạng hiện tại
        private int GetPlayerInfFromString(List<string> playerStringList)
        {
            int index = 0;
            for (int i = 0; i < playerStringList.Count; i++)
            {
                string[] array = playerStringList[i].Split(':');
                if (playerName == array[0])
                {
                    index = i;
                }
            }
            return index;
        }

        //Chỉnh sửa các thông tin cần thiết cho thanh tiến trình
        void SetupProgressBar()
        {
            //Đặt giá trị Maximum cho thanh tiến trình
            progressBar1.Maximum = 650;
            //Center vị trí thanh tiến trình
            int x = (this.Width - progressBar1.Width) / 2;
            int y = this.Height / 4;
            progressBar1.Location = new Point(x, y);
        }

        //Thiết lập giá trị Interval và trigger event khi thời gian Invertal kết thúc 
        void SetupTimer()
        {
            timer1 = new System.Windows.Forms.Timer
            {
                Interval = (int)timeout / 1000
            };
            timer1.Tick += new EventHandler(Timer1_Tick);
        }

        //Nhận thông tin cấu hình
        void ReceiveConfigInf()
        {
            byte[] data = new byte[1024];
            NetworkStream ns = new NetworkStream(client);
            StreamReader sr = new StreamReader(ns);
            StreamWriter sw = new StreamWriter(ns);
            byte[] packsize_byte = new byte[2];
            int recv = ns.Read(packsize_byte, 0, 2);
            int packsize = BitConverter.ToInt16(packsize_byte, 0);
            recv = ns.Read(data, 0, packsize);
            sr.Close();
            sw.Close();
            ns.Close();
            int place = 0;

            //Đổi các thông tin nhận được từ byte thành các dạng tương ứng
            //Thời gian trả lời câu hỏi
            timeout = BitConverter.ToInt32(data, place);
            place += 8;
            //Số lượng câu hỏi
            totalQuiz = BitConverter.ToInt32(data, place);
            place += 4;
            //Tên Player
            int playerNameLength = BitConverter.ToInt32(data, place);
            place += 4;
            playerName = Encoding.ASCII.GetString(data, place, playerNameLength);
            place += playerNameLength;
            Console.WriteLine("Total quiz: " + totalQuiz);
        }

        //Nhận bảng xếp hạng
        void ReceiveRank()
        {
            byte[] data = new byte[1024];
            NetworkStream ns = new NetworkStream(client);
            StreamReader sr = new StreamReader(ns);
            StreamWriter sw = new StreamWriter(ns);
            byte[] packsize_byte = new byte[2];
            int recv = ns.Read(packsize_byte, 0, 2);
            int packsize = BitConverter.ToInt16(packsize_byte, 0);
            recv = ns.Read(data, 0, packsize);
            sr.Close();
            sw.Close();
            ns.Close();
            if (recv != 0)
            {
                //Tạo bảng xếp hạng mới từ byte[] data vừa nhận được và hiển thị nó lên màn hình
                rank = new Rank(data);
                if (quiz_num != 0)
                {
                    ShowRanking();
                }
            }
        }


    }
}
