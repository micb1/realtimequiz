﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Server
{
    public partial class Form1 : Form
    {
        IPEndPoint ip;
        Socket server;
        List<Panel> panelList;
        List<Player> playerList;
        List<Quiz> quizList;
        Rank rank;
        int playerNum = 0;
        Thread listen;
        bool isStarted = false;
        int currentQuizIndex = 0;
        string pathToQuizFile = "./../../default.txt";
        public Form1()
        {
            InitializeComponent();

            //pEnterPort(); //return port=9999

            Listen();
            TextBox.CheckForIllegalCrossThreadCalls = false;
            RichTextBox.CheckForIllegalCrossThreadCalls = false;
            quizList = new List<Quiz>();
            playerList = new List<Player>();
            setQuizList(pathToQuizFile);
            SetupPanelList();
            SetupExportBtn();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e) { }

        //Hàm Listen() gắn địa chỉ cho socket chung của server và
        //tạo ra 1 thread để lắng nghe các yêu cầu kết nối của client
        void Listen()
        {
            //Khởi tạo danh sách playerList quản lý Player
            playerList = new List<Player>();
            //Bind địa chỉ cho socket của server
            ip = new IPEndPoint(IPAddress.Any, 9999);
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            server.Bind(ip);
            //Thread chờ yêu cầu kết nối
            listen = new Thread(() =>
            {
                try
                {
                    while (!isStarted)
                    {
                        server.Listen(1);
                        //Tạo Player mới tương ứng cho mỗi kết nối, trong Player này có socket kết nối với client
                        //Thêm Player vào danh sách playerList để dễ quản lý
                        Socket client = server.Accept();
                        Player new_player = new Player();
                        new_player.client = client;
                        //Đặt tên cho Player theo thứ tự kết nối
                        new_player.setName(playerList.Count.ToString());
                        playerList.Add(new_player);
                        //Gửi thông tin server cấu hình cho client: thời gian trả lời câu hỏi, tống số câu hỏi và tên của player được server đặt                       
                        SendConfigInf(10000, quizList.Count, new_player, "Player " + (playerList.Count - 1).ToString());
                        //Tạo thread receive để nhận câu trả lời của client
                        Thread receive = new Thread(receiveReply)
                        {
                            IsBackground = true
                        };
                        receive.Start(new_player);
                    }
                }
                catch (Exception e){}
            });
            listen.IsBackground = true;
            listen.Start();
            return;
        }

        void Close()
        {
            server.Close();
        }

        //Gửi câu hỏi cho tất cả client
        private void gameStart()
        {
            //Reset số lượng người chơi đã trả lời ở câu hỏi trước
            playerNum = 0;
            if (isStarted == false)
            {
                isStarted = true;
            }
            foreach (Player player in playerList)
            {
                SendQuiz(player);
            }
            button1.Visible = false;
        }

        //Tạo câu hỏi sẵn, sau này thay bằng import từ file
        private void setQuizList(string path)
        {
            // Khởi tạo list câu hỏi mới
            quizList = new List<Quiz>();

            FileStream fs = new FileStream(path, FileMode.Open);
            StreamReader rd = new StreamReader(fs, Encoding.UTF8);
            int i = 1;
            do
            {

                Quiz q = new Quiz();
                string question = "Question " + i + rd.ReadLine();
                string[] choices = new string[4];
                for (int j = 0; j <= 3; j++)
                {
                    choices[j] = rd.ReadLine();
                }
                string Answer = rd.ReadLine().Substring(8).Trim();
                for (int j = 0; j <= 3; j++)
                {
                    if (string.Compare(choices[j], Answer, true) == 0)
                    {
                        Answer = j.ToString();
                        break;
                    }

                }

                q.setQuestion(question);
                q.setChoices(choices);
                q.setAnswers(int.Parse(Answer));
                quizList.Add(q);
                i++;
            }
            while (rd.ReadLine() != null);

            rd.Close();
        }

        //Gửi câu hỏi cho một client cụ thể
        private void SendQuiz(Player player)
        {
            try
            {
                byte[] data = quizList[currentQuizIndex].GetBytes(playerList);
                SendComplexInf(data, player, quizList[currentQuizIndex].size);
            }
            catch (ArgumentException)
            {
                optionEndGame();   
            }
        }

        public void optionEndGame()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    optionEndGame();
                });
            }
            else
            {
                string caption = "Message";
                string message = "The game has ended" + Environment.NewLine;
                message += "Do you want to host a new game?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                // Displays the MessageBox.
                result = MessageBox.Show(message, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    // Closes the parent form.
                    this.Close();
                    Application.Restart();
                }
                else
                {
                    Dispose();
                }
            }
        }

        //Hàm chờ và nhận câu trả lời từ client
        private void receiveReply(object obj)
        {
            Player player = obj as Player;
            try
            {
                while (true)
                {
                    //Tham khảo code mẫu 07code bài Employee, dùng để nhận object
                    byte[] data = new byte[1024];
                    NetworkStream ns = new NetworkStream(player.client);
                    StreamReader sr = new StreamReader(ns);
                    StreamWriter sw = new StreamWriter(ns);
                    byte[] packsize_byte = new byte[2];
                    int recv = ns.Read(packsize_byte, 0, 2);
                    int packsize = BitConverter.ToInt16(packsize_byte, 0);
                    recv = ns.Read(data, 0, packsize);
                    Reply reply = new Reply(data);
                    sr.Close();
                    sw.Close();
                    ns.Close();
                    //Xử lý sau khi nhận được trả lời
                    if (recv != 0)
                    {
                        //Tính điểm dựa theo đáp án và thời gian trả lời
                        player.calcPoint(reply);
                        //Cập nhập số lượng Player đã trả lời trong câu hỏi này
                        playerNum++;
                        //Nếu toàn bộ Player đã trả lời thì qua câu tiếp theo
                        if (playerNum == playerList.Count)
                        {
                            //
                            currentQuizIndex++;
                            //Sắp xếp lại danh sách Player theo điểm giảm dần
                            playerList = playerList.OrderByDescending(list => list.getPoint()).ToList();
                            //Tạo bảng xếp hạng từ danh sách
                            rank = new Rank(playerList);
                            //Kiểm tra index của câu hỏi hiện tại trong danh sách câu hỏi
                            //Kết thúc game khi hết câu hỏi
                            if (currentQuizIndex > quizList.Count - 1)
                            {
                                endGame();
                            }
                            //Nếu còn câu hỏi, hiển thị bảng xếp hạng lên, sau đó gửi bảng xếp hạng này cho client
                            else
                            {
                                ShowRanking(false);
                                for (int i = 0; i < playerList.Count; i++)
                                {
                                    SendRank(playerList[i]);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e){}
        }

        //Xử lý kết thúc, chưa xong
        private void endGame()
        {
            ShowRanking(true);
            for (int i = 0; i < playerList.Count; i++)
            {
                SendRank(playerList[i]);
            }
        }


        //Tạo button Export bảng xếp hạng
        private void SetupExportBtn()
        {
            exportBtn.Size = new Size(button1.Width, button1.Height);
            exportBtn.Location = new Point(button1.Location.X - button1.Width - 10, button1.Location.Y);
            exportBtn.Click += new EventHandler(ExportRank);
        }

        //Hảm xử lý khi button Export được click
        private void ExportRank(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e) { }

        //Hảm xử lý khi button gửi câu hỏi được click
        private void button1_Click_1(object sender, EventArgs e)
        {
            if (rank != null)
            {
                //Ẩn bảng xếp hạng 
                HideRanking();
            }

            //ẩn setup thời gian và button import
            buttonImportQuizFile.Visible = false;

            //Bắt đầu gửi câu hỏi khác cho client
            gameStart();
        }


        private void SendConfigInf(long timeout, int totalQuiz, Player player, string playerName)
        {
            byte[] data = new byte[1024];
            int place = 0;
            Buffer.BlockCopy(BitConverter.GetBytes(timeout), 0, data, place, 8);
            place += 8;
            Buffer.BlockCopy(BitConverter.GetBytes(totalQuiz), 0, data, place, 4);
            place += 4;
            Buffer.BlockCopy(BitConverter.GetBytes(playerName.Length), 0, data, place, 4);
            place += 4;
            Buffer.BlockCopy(Encoding.ASCII.GetBytes(playerName), 0, data, place, playerName.Length);
            place += playerName.Length;
            SendComplexInf(data, player, place);
        }

        //Hàm chung dành cho việc gửi câu hỏi, gửi bảng xếp hạng 
        private void SendComplexInf(byte[] data, Player player, int size)
        {
            NetworkStream ns = new NetworkStream(player.client);
            StreamReader sr = new StreamReader(ns);
            StreamWriter sw = new StreamWriter(ns);
            byte[] packsize = new byte[2];
            packsize = BitConverter.GetBytes(size);
            ns.Write(packsize, 0, 2);
            ns.Write(data, 0, size);
            ns.Flush();
            sr.Close();
            sw.Close();
            ns.Close();
        }

        //Tạo bảng xếp hạng bằng các panel
        private void SetupPanelList()
        {
            int panelY = 0;
            //Thêm các panel này vào 1 list để quản lý
            panelList = new List<Panel>
            {
                panel1,
                panel2,
                panel3,
                panel4
            };
            //Chỉnh tham số cho từng panel
            for (int i = 0; i < panelList.Count; i++)
            {
                panelList[i].Size = new Size(rankPanel.Width, rankPanel.Height / 10);
                panelList[i].Left = (rankPanel.Width - panelList[i].Width) / 2;
                panelList[i].Top = (rankPanel.Height - panelList[i].Height) / 2 + panelY;
                panelY += panelList[i].Height;
                //Thêm label cho mỗi panel, label này dùng để hiện thông tin xếph hạng
                Label label = new Label
                {
                    Font = new Font("Cambria", 16),
                    AutoSize = false,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Dock = DockStyle.Fill,
                };
                panelList[i].Controls.Add(label);
            }
        }

        //Hiển thị bảng xếp hạng
        private void ShowRanking(bool isEnded)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    ShowRanking(isEnded);
                });
            }
            else
            {
                button1.Visible = true;
                updateButton1Text("Next Question");
                rankPanel.Visible = true;
                for (int i = 0; i < rank.getNum(); i++)
                {
                    panelList[i].Visible = true;
                    AddTextIntoPanel(panelList[i], i + 1 + ". " + rank.ranking[i]);
                }
                //Khi game kết thúc, ngoài hiển thị bảng xếp hạng thì hiển thị thêm button Export và button1
                if (isEnded == true)
                {
                    exportBtn.Visible = true;
                    updateButton1Text("End");
                }
            }
        }

        //Ẩn bảng xếp hạng
        private void HideRanking()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    HideRanking();
                });
            }
            else
            {
                rankPanel.Visible = false;
                for (int i = 0; i < rank.getNum(); i++)
                {
                    panelList[i].Visible = false;
                }
            }
        }
        //Ghi thông tin của string text vào panel xếp hạng
        private void AddTextIntoPanel(Panel panel, string text)
        {
            panel.Controls[0].Text = text;
            panel.Refresh();
        }

        //Gửi bảng xếp hạng để 1 Player cụ thể
        void SendRank(Player player)
        {
            byte[] data = rank.GetBytes();
            SendComplexInf(data, player, rank.getSize());
        }

        void updateButton1Text(string newText)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    updateButton1Text(newText);
                });
            }
            else
            {
                button1.Text = newText;
            }
        }

        /// <summary>
        /// Chọn file câu hỏi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImportQuizFile_Click(object sender, EventArgs e)
        {

            var FD = new System.Windows.Forms.OpenFileDialog();
            if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string fileToOpen = FD.FileName;

                System.IO.FileInfo File = new System.IO.FileInfo(FD.FileName);

                pathToQuizFile = FD.FileName;
                setQuizList(pathToQuizFile);

            }
        }

    }
}
