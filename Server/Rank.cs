﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server
{
    public class Rank
    {
        public List<string> ranking;
        public int num = 4;
        public int size = 0;
        public Rank()
        {
            ranking = new List<string>();
        }

        public Rank(List<string> playerStringList)
        {
            num = playerStringList.Count < num ? playerStringList.Count : num;
            ranking = playerStringList.GetRange(0, num);
        }

        public Rank(List<Player> playerList)
        {
            ranking = new List<string>();
            num = playerList.Count < num ? playerList.Count : num;
            for (int i = 0; i < playerList.Count; i++)
            {
                ranking.Add(playerList[i].toString());
            }
        }

        public int getNum()
        {
            return this.num;
        }

        public int getSize()
        {
            return this.size;
        }

        public string toString()
        {
            string s = "";
            for (int i = 0; i < num; i++)
            {
                s += ranking[i] + '\n';
            }
            return s;
        }

        public Rank(byte[] data)
        {
            ranking = new List<string>();
            int place = 0;
            num = BitConverter.ToInt32(data, place);
            place += 4;
            for (int i = 0; i < num; i++)
            {
                int playerStringlength = BitConverter.ToInt32(data, place);
                place += 4;
                string s = Encoding.ASCII.GetString(data, place, playerStringlength);
                ranking.Add(s);
                place += playerStringlength;
            }
            size = place;
        }

        public byte[] GetBytes()
        {
            byte[] data = new byte[1024];
            int place = 0;
            Buffer.BlockCopy(BitConverter.GetBytes(num), 0, data, place, 4);
            place += 4;
            for (int i = 0; i < num; i++)
            {
                string playerString = ranking[i];
                int playerStringLength = playerString.Length;
                Buffer.BlockCopy(BitConverter.GetBytes(playerStringLength), 0, data, place, 4);
                place += 4;
                Buffer.BlockCopy(Encoding.ASCII.GetBytes(playerString), 0, data, place, playerStringLength);
                place += playerStringLength;
            }
            size = place;
            return data;
        }
    }
}