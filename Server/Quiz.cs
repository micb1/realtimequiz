﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server
{
    public class Quiz
    {
        public string question;
        public string[] choices = new string[4];
        public byte[] data;
        public int answer = -1;
        public int size = 0;
        public int packsize = 0;
        public int choices_num = 4;

        public Quiz()
        {
            choices = new string[choices_num];
        }

        public void setQuestion(string question)
        {
            this.question = question;
        }

        public void setChoices(string[] choices)
        {
            if (choices.Length == 0 || choices == null)
            {
                Console.WriteLine("Fail");
                return;
            }
            this.choices = new string[choices.Length];
            choices.CopyTo(this.choices, 0);
        }

        public void setAnswers(int answer)
        {
            this.answer = answer;
        }

        public Quiz(byte[] data)
        {
            int place = 0;
            int question_size = BitConverter.ToInt32(data, place);
            place += 4;
            question = Encoding.ASCII.GetString(data, place, question_size);
            place += question_size;
            choices_num = BitConverter.ToInt32(data, place);
            place += 4;
            choices = new string[choices_num];
            for (int i = 0; i < choices_num; i++)
            {
                int choice_size = BitConverter.ToInt32(data, place);
                place += 4;
                choices[i] = Encoding.ASCII.GetString(data, place, choice_size);
                place += choice_size;
            }
            answer = BitConverter.ToInt32(data, place);
            place += 4;
            size = place;
        }

        public byte[] GetBytes(List<Player> playerList)
        {
            data = new byte[1024];
            int place = 0;
            Buffer.BlockCopy(BitConverter.GetBytes(question.Length), 0, data, place, 4);
            place += 4;
            Buffer.BlockCopy(Encoding.ASCII.GetBytes(this.question), 0, data, place, question.Length);
            place += question.Length;
            Buffer.BlockCopy(BitConverter.GetBytes(choices.Length), 0, data, place, 4);
            place += 4;
            for (int i = 0; i < choices_num; i++)
            {
                Buffer.BlockCopy(BitConverter.GetBytes(choices[i].Length), 0, data, place, 4);
                place += 4;
                Buffer.BlockCopy(Encoding.ASCII.GetBytes(this.choices[i]), 0, data, place, choices[i].Length);
                place += choices[i].Length;
            }
            Buffer.BlockCopy(BitConverter.GetBytes(answer), 0, data, place, 4);
            place += 4;
            size = place;
            return data;
        }

        public void printQuiz()
        {
            Console.WriteLine(toString());
        }

        public string toString()
        {
            string s = "";
            s += "Question: " + this.question + '\n';
            for (int i = 0; i < choices_num; i++)
            {
                s += "Choice " + i + " : " + this.choices[i] + '\n';
            }
            s += "Answer: " + choices[answer] + '\n';
            return s;
        }
    }
}