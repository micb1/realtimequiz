﻿using System;

namespace Server
{
    public class Reply
    {
        private bool isRight = false;
        private long timeTaken = 0;
        private int size;

        public Reply(bool isRight, long timeTaken)
        {
            this.isRight = isRight;
            this.timeTaken = timeTaken;
        }

        public Reply(byte[] data)
        {
            int place = 0;
            this.isRight = BitConverter.ToBoolean(data, place);
            place++;
            this.timeTaken = BitConverter.ToInt64(data, place);
            place += 8;
            this.size = place;
        }

        public void setIsRight(bool isRight)
        {
            this.isRight = isRight;
        }

        public void setTimeTaken(long timeTaken)
        {
            this.timeTaken = timeTaken;
        }

        public int getSize()
        {
            return size;
        }

        public byte[] getBytes()
        {
            byte[] data = new byte[1024];
            int place = 0;
            Buffer.BlockCopy(BitConverter.GetBytes(isRight), 0, data, place, 1);
            place++;
            Buffer.BlockCopy(BitConverter.GetBytes(timeTaken), 0, data, place, 8);
            place += 8;
            size = place;
            return data;
        }

        public string toString()
        {
            string s = "";
            s += "IsRight: " + isRight + '\n';
            s += "Time taken: " + timeTaken + '\n';
            s += "Size: " + size + '\n';
            return s;
        }

        public bool getIsRight()
        {
            return this.isRight;
        }

        public long getTimeTaken()
        {
            return this.timeTaken;
        }
    }
}