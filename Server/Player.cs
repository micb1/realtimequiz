﻿using System.Net.Sockets;

namespace Server
{
    public class Player
    {
        private string name = "";
        private long point = 0;
        private long recentlyTimeTaken;
        public Socket client;
        public void setName(string name)
        {
            this.name = name;
        }

        public void addPoint(long point)
        {
            this.point += point;
        }

        public string getName()
        {
            return this.name;
        }

        public long getPoint()
        {
            return this.point;
        }

        public void calcPoint(Reply reply)
        {
            recentlyTimeTaken = reply.getTimeTaken();
            if (reply.getIsRight() == true)
            {
                this.point += 1000;
                this.point -= reply.getTimeTaken() / 10;
            }
        }

        public string toString()
        {
            string s = "";
            s += "Player " + name + ": " + point + " - Time taken: " + recentlyTimeTaken + "ms" + '\n';
            return s;
        }
    }
}